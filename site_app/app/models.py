from django.db import models


# Create your models here.
class Pergunta(models.Model):
    pergunta_text = models.CharField(max_length=200)
    data_public = models.DateTimeField('Data de publicação')

    def __str__(self):
        return self.pergunta_text


class Resposta(models.Model):
    resposta_text = models.CharField(max_length=200)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    votos = models.IntegerField(default=0)

    def __str__(self):
        return self.resposta_text

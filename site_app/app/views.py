from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Pergunta, Resposta


class IndexView(ListView):
    template_name = 'app/index.html'
    context_object_name = 'final_lista_pergunta'

    def get_queryset(self):
        """ Retornando as 10 últimas perguntas publicadas """
        return Pergunta.objects.order_by('-data_public')[:10]


class DetalheView(DetailView):
    model = Pergunta
    template_name = 'app/detalhe.html'


class ResultadosView(DetalheView):
    model = Pergunta
    template_name = 'app/resultados.html'


def voto(request, pergunta_id):
    pergunta = get_object_or_404(Pergunta, pk=pergunta_id)
    try:
        selecao_resposta = pergunta.resposta_set.get(pk=request.POST['resposta'])
    except (KeyError, Resposta.DoesNotExist):
        return render(request, 'app/detalhe.html', {
            'pergunta': pergunta,
            'mensagem_erro': 'Você não selecionou uma resposta'
        })
    else:
        selecao_resposta.votos += 1
        selecao_resposta.save()
        return HttpResponseRedirect(reverse('app:resultados', args=(pergunta.id,)))
